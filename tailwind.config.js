// https://gka.github.io/palettes

const colors = {
  primary: {
    100: '#0b1923',
    200: '#0e1f2c',
    300: '#112331',
    400: '#162a39',
    500: '#20384a',
    600: '#2a4356',
    700: '#385367',
    800: '#4b6980',
    900: '#63839c',
  },
  secondary: {
    100: '#001f61',
    200: '#00367e',
    300: '#004e9b',
    400: '#0767b8',
    500: '#2e80d4',
    600: '#5398ef',
    700: '#74b3ff',
    800: '#95d1ff',
    900: '#b4eeff',
  },
  font: {
    light: '#f3f4f5',
    default: '#9ba3a8',
    dark: '#7d8488',
  },
};

module.exports = {
  purge: [],
  theme: {
    extend: {
      colors,
    },
  },
  variants: {},
  plugins: [],
};
