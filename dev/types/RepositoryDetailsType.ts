import { Type } from '@schematizer/schematizer';

export class RepositoryDetailsType {
  stars: number;

  forks: number;

  tags: string[];

  dependencies: number[];
}

export const repositoryDetailsType = new Type(RepositoryDetailsType).fields(() => ({
  stars: 'int',
  forks: 'int',
  tags: ['string'],
  dependencies: ['int'],
}));
