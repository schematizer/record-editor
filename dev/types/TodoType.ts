import { Type } from '@schematizer/schematizer';

export class TodoType {
  id: number;

  text: string;

  details?: string;

  done: boolean;
}

export const todoType = new Type(TodoType).fields(() => ({
  id: 'int',
  text: 'string',
  done: 'boolean',
  details: { type: 'string', nullable: true },
}));
