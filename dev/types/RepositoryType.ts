import { Type } from '@schematizer/schematizer';
import { RepositoryDetailsType } from './RepositoryDetailsType';

export class RepositoryType {
  id: string;

  title: string;

  details: RepositoryDetailsType;

  deprecated: boolean;
}

export const repositoryType = new Type(RepositoryType).fields(() => ({
  id: 'id',
  title: 'string',
  details: RepositoryDetailsType,
  deprecated: 'boolean',
}));
