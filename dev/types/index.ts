import { Type } from '@schematizer/schematizer';
import { ingredientType } from './IngredientType';
import { recipeType } from './RecipeType';
import { repositoryDetailsType } from './RepositoryDetailsType';
import { repositoryType } from './RepositoryType';
import { todoType } from './TodoType';

export * from './IngredientType';
export * from './RecipeType';
export * from './RepositoryDetailsType';
export * from './RepositoryType';
export * from './TodoType';

export const types: Type<any>[] = [
  ingredientType,
  recipeType,
  repositoryDetailsType,
  repositoryType,
  todoType,
];
