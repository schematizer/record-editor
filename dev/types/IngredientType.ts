import { Type } from '@schematizer/schematizer';

export class IngredientType {
  name: string;

  quantity: number;
}

export const ingredientType = new Type(IngredientType).fields(() => ({
  name: 'string',
  quantity: 'float',
}));
