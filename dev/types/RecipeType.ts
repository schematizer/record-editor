import { Type } from '@schematizer/schematizer';
import { IngredientType } from './IngredientType';

export class RecipeType {
  id: number;

  title: string;

  ingredients: IngredientType[];
}

export const recipeType = new Type(RecipeType).fields(() => ({
  id: 'int',
  title: 'string',
  ingredients: [IngredientType],
}));
