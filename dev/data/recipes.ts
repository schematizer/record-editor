import { RecipeType } from '../types';

export const recipes: RecipeType[] = [
  {
    id: 1,
    title: 'To do list',
    ingredients: [
      { quantity: 4, name: 'years of university' },
      { quantity: 8, name: 'days trying it' },
      { quantity: 5, name: 'minutes finding on stack overflow' },
      { quantity: 1, name: 'copy' },
      { quantity: 1, name: 'paste' },
    ],
  },
  {
    id: 2,
    title: 'A lot of cookies',
    ingredients: [
      { quantity: 1, name: 'Google Chrome tab' },
      { quantity: 1, name: 'Facebook account' },
      { quantity: 1, name: 'seconds visiting Facebook' },
    ],
  },
];
