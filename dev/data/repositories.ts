import { RepositoryType } from '../types';

export const repositories: RepositoryType [] = [
  {
    id: 'facebook/react',
    title: 'React',
    details: {
      forks: 30400,
      stars: 155000,
      tags: ['react'],
      dependencies: [1, 2, 3],
    },
    deprecated: false,
  },
  {
    id: 'vuejs/vue',
    title: 'Vue.js',
    details: {
      forks: 26200,
      stars: 171000,
      tags: ['vue'],
      dependencies: [1, 2, 3],
    },
    deprecated: false,
  },
  {
    id: 'sveltejs/svelte',
    title: 'Svelte',
    details: {
      forks: 1700,
      stars: 36600,
      tags: ['UI', 'framework', 'templates', 'templating'],
      dependencies: [1, 2, 3],
    },
    deprecated: false,
  },
];
