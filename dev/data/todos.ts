import { TodoType } from '../types';

export const todos: TodoType[] = [
  { id: 1, text: 'Add some todos', done: true },
  { id: 2, text: 'buy happiness', done: false },
  { id: 3, text: 'learn CD/CI', done: false },
];
