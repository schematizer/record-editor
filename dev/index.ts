import { createServer } from './createServer';

const PORT = Number(process.env.PORT) || 6969;

(async () => {
  const { apollo, app } = await createServer();

  app.listen(PORT, '0.0.0.0', () => {
    console.log('graphql: ', `http://localhost:${PORT}${apollo.graphqlPath}`);
    console.log('editor:', `http://localhost:${PORT}/editor`);
  });
})();
