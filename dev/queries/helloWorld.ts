import { Fn } from '@schematizer/schematizer';

export const helloWorld = Fn('string', () => () => 'Hello World!');
