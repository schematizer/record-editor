import { Queries } from '@schematizer/schematizer';
import { helloWorld } from './helloWorld';

export const queries = new Queries({
  helloWorld,
});
