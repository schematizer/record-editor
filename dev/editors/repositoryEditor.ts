import { EditorConfig } from '../../src';
import { RepositoryType } from '../types';
import { repositories } from '../data/repositories';

export const repositoryEditor: EditorConfig<RepositoryType> = {
  id: 'repository',
  type: RepositoryType,
  name: 'Reposiories',
  idField: 'id',
  search: () => repositories,
};
