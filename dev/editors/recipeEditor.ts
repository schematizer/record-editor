import { EditorConfig } from '../../src';
import { RecipeType } from '../types';
import { recipes } from '../data/recipes';

export const recipeEditor: EditorConfig<RecipeType> = {
  id: 'recipe',
  type: RecipeType,
  name: 'Recipes',
  idField: 'id',
  search: () => recipes,
};
