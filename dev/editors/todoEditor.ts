import { EditorConfig } from '../../src';
import { TodoType } from '../types';
import { todos } from '../data/todos';

export const todoEditor: EditorConfig<TodoType> = {
  id: 'todos',
  type: TodoType,
  name: 'Todo list',
  idField: 'id',
  search: () => todos,
};
