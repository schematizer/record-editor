import { EditorConfig } from '../../src';
import { recipeEditor } from './recipeEditor';
import { repositoryEditor } from './repositoryEditor';
import { todoEditor } from './todoEditor';

export const editors: EditorConfig[] = [
  todoEditor,
  recipeEditor,
  repositoryEditor,
];
