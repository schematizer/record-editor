import { schematize } from '@schematizer/schematizer';
import { RecordEditor } from '../src';
import { types } from './types';
import { queries } from './queries';
import { mutations } from './mutations';
import { inputs } from './inputs';
import { editors } from './editors';

export function createSchema() {
  return schematize(
    ...types,
    ...inputs,
    queries,
    mutations,
    RecordEditor(editors),
  );
}
