import { ApolloServer } from 'apollo-server-express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as express from 'express';
import { createSchema } from './createSchema';
import { PageMiddleware } from '../src';

export async function createServer() {
  const app = express();
  app.use(bodyParser.json({
    limit: '100mb',
  }));
  app.use(cookieParser());

  const schema = await createSchema();
  const apollo = new ApolloServer({
    schema,
  });
  apollo.applyMiddleware({ app });

  PageMiddleware({ app });

  return { apollo, app };
}
