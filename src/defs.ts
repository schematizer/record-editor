import { ClassDef, RecursivePartial } from '@schematizer/schematizer';

export interface SearchRecordsArg {
  take: number;
  offset: number;
}
export type SearchRecordsFunction<Type> =
  (arg: SearchRecordsArg) => RecursivePartial<Type>[] | Promise<RecursivePartial<Type>[]>;

export type AddRecordFunction<Input> =
  (record: RecursivePartial<Input>) => void | Promise<void>;

export type UpdateRecordFunction<Input> =
  (record: RecursivePartial<Input>) => void | Promise<void>;

export type RemoveRecordsFunction<Id> = (ids: Id[]) => void | Promise<void>;

export interface EditorConfig<Type = any, Input = any> {
  id: string;
  name: string;
  type: ClassDef<Type>;
  input?: ClassDef<Input>;
  idField: keyof Type;
  search: SearchRecordsFunction<Type>;
  add?: AddRecordFunction<Input>;
  update?: UpdateRecordFunction<Input>;
  remove?: RemoveRecordsFunction<Type[keyof Type]>
}
