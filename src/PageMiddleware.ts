import { resolve } from 'path';
import * as express from 'express';

export interface PageMiddlewareArg {
  app: express.Application;
}

export function PageMiddleware({ app }: PageMiddlewareArg) {
  const dir = express.static(resolve('ui', 'public'));
  const spa = express.static(resolve('ui', 'public', 'index.html'));

  app.use('/editor', dir);
  app.use('/editor/*', spa);

  return app;
}
