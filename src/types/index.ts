import { Type } from '@schematizer/schematizer';
import { editorType } from './Editor';

export * from './Editor';

export const types: Type[] = [editorType];
