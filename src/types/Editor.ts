import { Type } from '@schematizer/schematizer';

export class Editor {
  id: string;

  name: string;

  type: string;

  idField: string;
}

export const editorType = new Type(Editor).as('_Editor').fields(() => ({
  id: 'id',
  name: 'string',
  type: 'string',
  idField: 'string',
}));
