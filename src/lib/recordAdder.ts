import { Arg, Mutations, Fn } from '@schematizer/schematizer';
import { EditorConfig } from '../defs';

export function recordAdder(config: EditorConfig<any>) {
  if (!config.add) return null;

  const mutationName = `_add_${config.id}`;

  const mutations = new Mutations({
    [mutationName]: Fn(config.type, ({
      input = Arg([config.input]),
    }) => () => config.add(input)),
  });

  return mutations;
}
