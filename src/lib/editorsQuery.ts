import { Queries, Fn } from '@schematizer/schematizer';
import { Editor } from '../types';

export function editorsQuery(editors: Editor[]) {
  const queries = new Queries({
    _editors: Fn([Editor], () => () => editors),
  });

  return queries;
}
