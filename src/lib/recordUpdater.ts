import { Arg, Mutations, Fn } from '@schematizer/schematizer';
import { EditorConfig } from '../defs';

export function recordUpdater(config: EditorConfig<any>) {
  if (!config.update) return null;

  const mutationName = `_update_${config.id}`;

  const mutations = new Mutations({
    [mutationName]: Fn(config.type, ({
      input = Arg([config.input]),
    }) => () => config.update(input)),
  });

  return mutations;
}
