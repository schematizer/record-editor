import { DefinitionStore } from '@schematizer/schematizer/dist/DefinitionStore';
// TODO: move "Schematizable" to types.ts
import { Schematizable } from '@schematizer/schematizer/dist/utils/separateSchematizbles';
import { recordAdder } from './recordAdder';
import { recordSearchser } from './recordSearcher';
import { recordUpdater } from './recordUpdater';
import { EditorConfig } from '../defs';
import { Editor } from '../types';
import { recordRemover } from './recordRemover';

function validateId(id: string) {
  const pattern = /^[_a-zA-Z][_a-zA-Z0-9]*$/;
  if (!pattern.test(id)) {
    throw new Error(`Ids must match ${pattern} but "${id}" does not.`);
  }
}

export function createEditor(config: EditorConfig<any>, store: DefinitionStore) {
  validateId(config.id);

  const type = store.getType(config.type);
  const data: Editor = {
    id: config.id,
    name: config.name,
    type: type.alias,
    idField: config.idField as string,
  };

  const schematizables: Schematizable[] = [];

  const searcher = recordSearchser(config);
  schematizables.push(searcher);

  const updateRecords = recordUpdater(config);
  if (updateRecords) {
    schematizables.push(updateRecords);
  }

  const adder = recordAdder(config);
  if (adder) {
    schematizables.push(adder);
  }

  const remover = recordRemover(config);
  if (remover) {
    schematizables.push(remover);
  }

  return { data, schematizables };
}
