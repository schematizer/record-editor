import { Arg, Mutations, Fn } from '@schematizer/schematizer';
import { EditorConfig } from '../defs';

export function recordRemover(config: EditorConfig<any>) {
  if (!config.remove) return null;

  const mutationName = `_remove_${config.id}`;

  const mutations = new Mutations({
    [mutationName]: Fn(config.type, ({
      input = Arg([config.input]),
    }) => () => config.remove(input)),
  });

  return mutations;
}
