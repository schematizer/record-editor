import { Arg, Queries, Fn } from '@schematizer/schematizer';
import { EditorConfig } from '../defs';

export function recordSearchser(config: EditorConfig<any>) {
  const queryName = `_search_${config.id}`;

  const queries = new Queries({
    [queryName]: Fn([config.type], ({
      offset = Arg({ type: 'int', nullable: true }),
      take = Arg({ type: 'int', nullable: true }),
    }) => () => config.search({ offset, take })),
  });

  return queries;
}
