import { SchematizerPlugin } from '@schematizer/schematizer';
import { editorsQuery } from './lib/editorsQuery';
import { createEditor } from './lib/createEditor';
import { EditorConfig } from './defs';
import { Editor, editorType } from './types';

export const RecordEditor = (editors: EditorConfig<any>[]) => {
  const plugin: SchematizerPlugin = ({ use, store }) => {
    const editorsData: Editor[] = [];

    for (const editor of editors) {
      const { data, schematizables } = createEditor(editor, store);

      editorsData.push(data);
      use(...schematizables);
    }

    use(editorType);
    use(editorsQuery(editorsData));
  };

  return plugin;
};
