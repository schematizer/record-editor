import { setContext, getContext } from 'svelte';
import ApolloClient, { InMemoryCache } from 'apollo-boost';
import { query as apolloQuery, mutate as apolloMutate } from 'svelte-apollo';

export function createClient() {
  /**
   * @type { ApolloClient }
   */
  const client = new ApolloClient({
    uri: '/graphql',
    cache: new InMemoryCache({
      addTypename: false,
    }),
  });

  /**
   * @param { import('graphql').DocumentNode } query
   * @param { import('apollo-boost').WatchQueryOptions } options
   */
  function queryFn(query, options) {
    return apolloQuery(client, { query, ...options });
  }

  /**
   * @param { import('graphql').DocumentNode } mutation
   * @param { import('apollo-boost').MutationOptions } options
   */
  function mutateFn(mutation, options) {
    return apolloMutate(client, { mutation, ...options });
  }

  return {
    client,
    query: queryFn,
    mutate: mutateFn,
  };
}

const CLIENT_KEY = 'apollo-client';

export function setClient(client) {
  setContext(CLIENT_KEY, client);
}

export function getClient() {
  /**
   * @type { import('apollo-boost').ApolloClient }
   */
  const client = getContext(CLIENT_KEY);

  return client;
}
