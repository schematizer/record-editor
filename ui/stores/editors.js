/* eslint-disable no-underscore-dangle */
import { writable } from 'svelte/store';
import { EDITORS_QUERY } from '../queries';
import parseEditorsData from '../utils/parseEditorsData';
import parseTypes from '../utils/parseTypes';

function createEditors() {
  const { subscribe, set } = writable({
    loading: true,
    error: undefined,
    editors: undefined,
    types: undefined,
  });

  function init(client) {
    client.query(EDITORS_QUERY).subscribe((query) => {
      query.then(({ data }) => {
        const types = parseTypes(data.__schema.types);
        set({
          editors: parseEditorsData(data),
          loading: false,
          types,
        });
      }).catch((error) => {
        set({
          error,
          loading: false,
        });
      });
    });
  }

  return {
    subscribe,
    init,
  };
}

export const editorsStore = createEditors();
