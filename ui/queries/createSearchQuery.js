import { selectType } from '../utils/buildSelection';

export function createSearchQuery(editor, schema) {
  /**
   * @type { import('graphql').DocumentNode }
   */
  const query = {
    kind: 'Document',
    definitions: [{
      kind: 'OperationDefinition',
      operation: 'query',
      variableDefinitions: [],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: { kind: 'Name', value: editor.searchQuery },
            arguments: [],
            directives: [],
            selectionSet: selectType(editor.type, schema),
          },
        ],
      },
    }],
  };

  return query;
}
