export default function getPath(location) {
  const [, path] = /^\/editor\/?(.*)$/.exec(location.pathname);

  return path || '/';
}
