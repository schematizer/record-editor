function isNotNull(node) {
  if (node.kind === 'NON_NULL') {
    return true;
  }
  if (node.type || node.ofType) {
    return isNotNull(node.type || node.ofType);
  }

  return false;
}

function getListNode(node) {
  if (node.kind === 'LIST') {
    return node;
  }
  if (node.type || node.ofType) {
    return getListNode(node.type || node.ofType);
  }

  return false;
}

function getScalar(node) {
  if (node.kind === 'SCALAR') {
    return node.name;
  }
  if (node.type || node.ofType) {
    return getScalar(node.type || node.ofType);
  }

  return null;
}

function getType(node) {
  if (node.kind === 'OBJECT') {
    return node.name;
  }
  if (node.type || node.ofType) {
    return getType(node.type || node.ofType);
  }

  return null;
}

/*
interface Type {
  type: 'type';
  name: string;
  nullable: boolean;
  fields?: Record<string, Node>;
}

interface Scalar {
  type: 'scalar';
  name: string;
  nullable: boolean;
}

interface List {
  type: 'list';
  nullable: boolean;
  ofType: Node;
}

type Node = Type | Scalar | List;

*/

function parseField(node) {
  const nullable = !isNotNull(node);

  const listNode = getListNode(node);
  if (listNode) {
    return {
      type: 'list',
      ofType: parseField(listNode.ofType),
      nullable,
    };
  }

  const scalarName = getScalar(node);
  if (scalarName) {
    return {
      type: 'scalar',
      name: scalarName,
      nullable,
    };
  }

  const typeName = getType(node);
  if (typeName) {
    return {
      type: 'type',
      name: typeName,
      fields: null,
      nullable,
    };
  }

  throw new Error(`Unexpected node "${node.name}" of kind "${node.kind}"`);
}

function parseType(node) {
  if (node.kind !== 'OBJECT') {
    throw new Error(`Unexpected node ${node.kind} at type selection`);
  }

  return {
    type: 'type',
    name: node.name,
    nullable: false,
    fields: node.fields.reduce((result, fieldNode) => ({
      ...result,
      [fieldNode.name]: parseField(fieldNode),
    }), {}),
  };
}

export default function parseTypes(allRawTypes) {
  const rawTypes = allRawTypes.filter((rawType) => rawType.kind === 'OBJECT' && !rawType.name.startsWith('__'));

  return rawTypes.map((rawType) => parseType(rawType));
}
