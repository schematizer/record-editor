/* eslint-disable @typescript-eslint/no-use-before-define */

function getFieldTypeName(field) {
  if (field.type === 'list') {
    return getFieldTypeName(field.ofType);
  }
  if (field.type === 'type') {
    return field.name;
  }
  return null;
}

function selectField(name, field, types) {
  const typeName = getFieldTypeName(field);

  return {
    kind: 'Field',
    name: { kind: 'Name', value: name },
    arguments: [],
    directives: [],
    selectionSet: typeName ? selectType(typeName, types) : undefined,
  };
}

export function selectType(typeName, types) {
  const type = types.find((t) => t.name === typeName);

  return {
    kind: 'SelectionSet',
    selections: Object
      .keys(type.fields)
      .map((fieldName) => selectField(fieldName, type.fields[fieldName], types)),
  };
}
