// function getMutations(schema) {
//   if (!schema.mutationType) {
//     return [];
//   }

//   const mutationField = schema.mutationType.name;
//   const mutationType = schema.types.find((t) => t.name === mutationField);
//   if (!mutationType) {
//     throw new Error(`Error: type "${mutationField}" not found`);
//   }

//   return mutationType.fields;
// }

// function getRemoveMutation(schema, id) {
//   const mutations = getMutations(schema);
// }

export default function parseEditorsData({ _editors }) {
  return _editors.map((editor) => ({
    ...editor,
    searchQuery: `_search_${editor.id}`,
  }));
}
